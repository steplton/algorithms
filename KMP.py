t = 'лилила'
p = [0]*len(t)
j = 0
i = 1

# 1 Этап
# Формируем массив Пи
while i < len(t):
    if t[i] != t[j]:
        if j == 0:
            p[i] = 0
            i+=1
        else:
            j = p[j-1]
    else:
        p[i] = j+1
        i+=1
        j+=1

# 2 Этап
# Организовываем поиск
a = 'лилилось лилилась'
i = 0
j = 0
n = len(a)
m = len(t) 

while i < n:
    if a[i] == t[j]:
        i+=1
        j+=1
        if j == m:
            print(f'Нашли совпадение! Начальный индекс: {i - m}, конечный: {i-1}')
            print(a[9:15])
            break
    else:
        if j>0:
            j = p[j-1]
        else:
            i+=1
if i == n:
    print('Совпадений не найдено')